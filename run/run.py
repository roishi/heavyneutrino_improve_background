import ROOT
ROOT.gROOT.SetBatch()
import re
import pprint
import subprocess
import sys
import os
import time

import optparse
parser = optparse.OptionParser()
parser.add_option('-d', '--data', dest = 'data', action = 'store_true', default = False, help = 'Enable or diable of siplotting data simultaneously. False by default')
(options, args) = parser.parse_args()

class Improve_SignalEfficiency(object):
    def __init__(self, file_directory = None, suffix_bkg = None, suffix_sig = None, suffix_data = None):
        self.file_directory = file_directory
        self.suffixes = {'BKG': suffix_bkg,
                         'SIG': suffix_sig,
                         'DATA': suffix_data}

    def Check_Files(self, mode = ''):
        command = 'ls %s/%s' % (self.file_directory, self.suffixes[mode])
        process_out = subprocess.Popen(command.split(), stdout = subprocess.PIPE).communicate()
        process_out_arrange = process_out[0].split('\n')
        process_out_arrange = process_out_arrange[:-1]
        return process_out_arrange

    def FileNameParser(self, filename):
        filename_elements = filename.replace('.root', '').split('_')
        integrated_luminosity = 0
        sorted_elements = []
        for element in filename_elements:
            if 'Luminosity' in element:
                integrated_luminosity = float(element.split('-')[1])
            else:
                sorted_elements.append(element)
        sorted_elements.sort()
        sorted_elements_as_str = ''
        for element in sorted_elements:
            sorted_elements_as_str += '%s_' % (element)

        return sorted_elements_as_str[:-1], integrated_luminosity

    def ConfigParser(self, configall = None):
        config_dict = {}
        configall_element = configall.split('_')
        for element in configall_element:
            key_value = element.split('-')
            value = ''
            if 'greater' in key_value[1]:
                value = float(key_value[1].replace('greater', ''))
            elif 'smaller' in key_value[1]:
                value = float(key_value[1].replace('smaller', '-'))
            else:
                value = key_value[1]
            config_dict[key_value[0]] = value
        return config_dict

    def Main(self):
        FileInfoAll = {} # Every information is stored in this dict, print this if you messed up.
        for mode in self.suffixes:
            files = self.Check_Files(mode = mode)
            for file in files:
                fileconfig, integrated_luminosity = self.FileNameParser(file)
                file = '%s/%s/%s' % (self.file_directory, self.suffixes[mode], file)
                if fileconfig in FileInfoAll:
                    FileInfoAll[fileconfig][mode] = {'file': file, 'luminosity': integrated_luminosity}
                else:
                    FileInfoAll[fileconfig] = {}
                    FileInfoAll[fileconfig][mode] = {'file': file, 'luminosity': integrated_luminosity}

        ROOT.gROOT.LoadMacro('../code/analysis.cpp+')
        analysis = ROOT.analysis()
        outputfile_name = ''
        if options.data:
            outputfile_name = 'figures/Signal_%s-Background_%s-Data_%s.root' % (self.suffixes['SIG'], self.suffixes['BKG'], self.suffixes['DATA'])
            analysis.Figure_Prepare(outputfile_name)
        else:
            outputfile_name = 'figures/Signal_%s-Background_%s.root' % (self.suffixes['SIG'], self.suffixes['BKG'])
            analysis.Figure_Prepare(outputfile_name)

        FileInfoAll_keys = FileInfoAll.keys()
        FileInfoAll_keys.sort()

        for i_config, config in enumerate(FileInfoAll_keys):
            print('%4d/%4d : %s is processed.' % (i_config+1, len(FileInfoAll_keys)+1, config))
            if options.data:
                if not 'BKG' in FileInfoAll[config] or not 'SIG' in FileInfoAll[config] or not 'DATA' in FileInfoAll[config]:
                    continue
                config_as_dict = self.ConfigParser(configall = config)
                # For 1 el + 1lj channel
                analysis.Read_from_file_SIG_one_reco(FileInfoAll[config]['SIG']['file'])
                analysis.Read_from_file_BKG_one_reco(FileInfoAll[config]['BKG']['file'])
                analysis.Read_from_file_DATA_one_reco(FileInfoAll[config]['DATA']['file'])
                analysis.CheckContent_SIG_one_reco(FileInfoAll[config]['SIG']['luminosity'], FileInfoAll[config]['DATA']['luminosity'])
                analysis.CheckContent_BKG_one_reco(FileInfoAll[config]['BKG']['luminosity'], FileInfoAll[config]['DATA']['luminosity'])
                analysis.CheckContent_DATA_one_reco()
                analysis.Calculate_ExpPZ_one_reco()
                # For 2 el + 1lj channel
                # analysis.Read_from_file_SIG_two_reco(FileInfoAll[config]['SIG']['file'])
                # analysis.Read_from_file_BKG_two_reco(FileInfoAll[config]['BKG']['file'])
                # analysis.Read_from_file_DATA_two_reco(FileInfoAll[config]['DATA']['file'])
                # analysis.CheckContent_SIG_two_reco(FileInfoAll[config]['SIG']['luminosity'], FileInfoAll[config]['DATA']['luminosity'])
                # analysis.CheckContent_BKG_two_reco(FileInfoAll[config]['BKG']['luminosity'], FileInfoAll[config]['DATA']['luminosity'])
                # analysis.CheckContent_DATA_two_reco()
                # analysis.Calculate_ExpPZ_two_reco()
                # Common
                analysis.Append_Configall(config)
                analysis.SetConfigSetting(float(config_as_dict['MET']),
                                          float(config_as_dict['METsig']),
                                          config_as_dict['Isolation'], 
                                          config_as_dict['Quality'],
                                          float(config_as_dict['fatjetmassuppercut']), # # Mass of fat jet corresponding leading electron
                                          float(config_as_dict['leadingelpt']),
                                          float(config_as_dict['deltaeta']),
                                          float(config_as_dict['deltar']),
                                          int(config_as_dict['nfatjet']),
                                          float(config_as_dict['fatjetmass']), # Mass of fat jet corresponding the decay products of heavy neutrino
                )
                analysis.Figure_Combine_BKG_SIG_DATA_one_reco()
                # analysis.Figure_Combine_BKG_SIG_DATA_two_reco()
                analysis.Figure_Efficiencies_BKG_SIG_one_reco()
                analysis.Reset_ALL_one_reco_BKG_SIG_DATA()
                # analysis.Reset_ALL_two_reco()
            else:
                if not 'BKG' in FileInfoAll[config] or not 'SIG' in FileInfoAll[config]:
                    continue
                config_as_dict = self.ConfigParser(configall = config)
                # For 1 el + 1lj channel
                analysis.Read_from_file_SIG_one_reco(FileInfoAll[config]['SIG']['file'])
                analysis.Read_from_file_BKG_one_reco(FileInfoAll[config]['BKG']['file'])
                analysis.CheckContent_SIG_one_reco(FileInfoAll[config]['SIG']['luminosity'], FileInfoAll[config]['BKG']['luminosity'])
                analysis.CheckContent_BKG_one_reco(FileInfoAll[config]['BKG']['luminosity'], FileInfoAll[config]['BKG']['luminosity'])
                analysis.Calculate_ExpPZ_one_reco()
                # For 2 el + 1lj channel
                # analysis.Read_from_file_SIG_two_reco(FileInfoAll[config]['SIG']['file'])
                # analysis.Read_from_file_BKG_two_reco(FileInfoAll[config]['BKG']['file'])
                # analysis.CheckContent_SIG_two_reco(FileInfoAll[config]['SIG']['luminosity'], FileInfoAll[config]['BKG']['luminosity'])
                # analysis.CheckContent_BKG_two_reco(FileInfoAll[config]['BKG']['luminosity'], FileInfoAll[config]['BKG']['luminosity'])
                # analysis.Calculate_ExpPZ_two_reco()
                # Common
                analysis.Append_Configall(config)
                analysis.SetConfigSetting(float(config_as_dict['MET']),
                                          float(config_as_dict['METsig']),
                                          config_as_dict['Isolation'],
                                          config_as_dict['Quality'],
                                          float(config_as_dict['fatjetmassuppercut']), # Mass of fat jet corresponding leading electron
                                          float(config_as_dict['leadingelpt']),
                                          float(config_as_dict['deltaeta']),
                                          float(config_as_dict['deltar']),
                                          int(config_as_dict['nfatjet']),
                                          float(config_as_dict['fatjetmass']), # Mass of fat jet corresponding the decay products of heavy neutrino
                )
                analysis.Figure_Combine_BKG_SIG_one_reco()
                # analysis.Figure_Combine_BKG_SIG_two_reco()
                analysis.Figure_Efficiencies_BKG_SIG_one_reco()
                analysis.Reset_ALL_one_reco_BKG_SIG()
                # analysis.Reset_ALL_two_reco()

        analysis.Figure_Close()
        print('Every plots were stored in %s' % (outputfile_name))

if __name__ == '__main__':
    start_time = time.time()

    file_directory = '/data/data1/zp/roishi/HeavyNeutrino/heavyneutrino_plotter_scripts/condor/figures'

    suffix_bkg = 'MC16d_20201215MC16a_20210104' # Standard OR, 80fb-1
    suffix_sig = 'MC16e_20210201' # Standard OR
    suffix_data = 'Data_20210108' # Standard OR, Year 15, 16, 17

    suffix_bkg = 'MC16a_2021-01-30MC16d_2021-01-30MC16e_2021-01-30' # Special OR
    suffix_sig = 'MC16e_20210201' # Special OR (5TeV - 200GeV)
    # suffix_sig = 'MC16d_20210201' # Special OR (5TeV - 500GeV)
    suffix_data = 'DATA_2021-01-30' # Special OR, Year 15, 16, 17, 18

    suffix_bkg = 'MC16a_2021-03-06MC16d_2021-03-06MC16e_2021-03-06' # Special OR with b-tagging
    suffix_sig = 'MC16e_20210308' # Special OR witth b-tagging (5TeV - 200GeV)
    suffix_data = 'DATA_2021-03-06' # Special OR with b-tagging, Year 15, 16, 17, 18

    suffix_bkg = 'MC16a_2021-03-24MC16d_2021-03-24MC16e_2021-03-24' # Special OR with b-tagging DL1
    suffix_sig = 'MC16e_20210308' # Special OR with b-tagging DL1 (5TeV - 200GeV)
    suffix_data = 'DATA_2021-03-24' # Special OR with b-tagging DL1, Year 15, 16, 17, 18

    obj = Improve_SignalEfficiency(file_directory = file_directory, suffix_bkg = suffix_bkg, suffix_sig = suffix_sig, suffix_data = suffix_data)
    obj.Main()

    end_time = time.time()
    print('Elapsed time: %.3f' % (end_time - start_time))
    

#ifndef HEAVYNEUTRINO_IMPROVE_BACKGROUND_ANALYSIS_H
#define HEAVYNEUTRINO_IMPROVE_BACKGROUND_ANALYSIS_H

#include <iostream>
#include <vector>
#include <cmath>
#include <memory>
#include <map>

#include "TLorentzVector.h"
#include "TGraph.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TH2D.h"
#include "TGraph2D.h"
#include "THStack.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TDirectoryFile.h"
#include "TPaveText.h"
#include "TLatex.h"

#include "RootUtils/AtlasUtils.h"
#include "RootUtils/AtlasUtils.C"
#include "RootUtils/AtlasLabels.h"
#include "RootUtils/AtlasLabels.C"
#include "RootUtils/AtlasStyle.h"

#include "RooStats/RooStatsUtils.h"
#include "RooStats/NumberCountingUtils.h"

class analysis{
 public:
  // constructor
  analysis(){};

  // Common tools
 public:
  void Append_Configall(TString configall);
  void UnSetConfigSetting();
  void SetConfigSetting(float MET_cut,
                        float METsignificant_cut,
                        TString Isolation,
                        TString Quality,
                        float fatjet_mass_upper_cut,
                        float leading_electron_pt_cut,
                        float delta_eta_cut,
                        float delta_R_cut,
                        int number_of_fatjet,
                        float fatjet_mass);
  double Get_Pvalue(float n_signal, float n_background);
  double Get_Zvalue(float n_signal, float n_background);
  float ErrorPropagation_sum(float sigma_one, float sigma_two);
  float ErrorPropagation_division(float mu_one, float mu_two, float sigma_one, float sigma_two);

 private:
  TFile *file_bkg_one_reco;
  TFile *file_sig_one_reco;
  TFile *file_data_one_reco;

  TFile *file_bkg_two_reco;
  TFile *file_sig_two_reco;
  TFile *file_data_two_reco;

 private:
  float signal_region_mass_min = 4000; // in GeV
  std::vector<TString> configall_vector;
  float config_MET_cut = 0;
  float config_METsignificant_cut = 0;
  TString config_Isolation = "";
  TString config_Quality = "";
  float config_fatjet_mass_upper_cut = 0; // Mass of fat jet corresponding leading electron
  float config_leading_electron_pt_cut = 0;
  float config_delta_eta_cut = 0;
  float config_delta_R_cut = 0;
  int config_number_of_fatjet = 0;
  float config_fatjet_mass = 0; // Mass of fat jet corresponding the decay products of heavy neutrino

  // For 1el + 1lj channel
 public:
  void Reset_ALL_one_reco_BKG_SIG();
  void Reset_ALL_one_reco_BKG_SIG_DATA();
  void Read_from_file_BKG_one_reco(TString input_file_bkg);
  void Read_from_file_SIG_one_reco(TString input_file_sig);
  void Read_from_file_DATA_one_reco(TString input_file_data);
  void CheckContent_BKG_one_reco(float integral_lumi_origin, float integral_lumi_ref);
  void CheckContent_SIG_one_reco(float integral_lumi_origin, float integral_lumi_ref);
  void CheckContent_DATA_one_reco();

  void CheckError_BKG_one_reco(float integral_lumi_origin, float integral_lumi_ref);
  void CheckError_SIG_one_reco(float integral_lumi_origin, float integral_lumi_ref);
  void CheckError_DATA_one_reco();

  void Calculate_ExpPZ_one_reco();

 private:
  /* TString plot_target = "ljet_el_mass_of_"; */
  /* TString plot_suffix = "_one_reco"; */
  /* TString plot_target = "el_pt_of_"; */
  /* TString plot_suffix = "_one_reco"; */
  /* TString plot_target = "AdditionalCut_DeltaEta_Fatjet_Leadingel_"; */
  /* TString plot_suffix = ""; */
  /* TString plot_target = "MissingET_one_reco_"; */
  /* TString plot_suffix = ""; */
  /* TString plot_target = "lj_multiplicity_of_"; */
  /* TString plot_suffix = "_one_reco"; */
  TString plot_target = "W_jets_smallest_dr_W_sjet_of_";
  TString plot_suffix = "_one_reco";
  /* TString plot_target = "W_jets_e_angle_rest_frame_of_"; */
  /* TString plot_suffix = "_one_reco"; */

 private:
  TH1F *ljet_el_mass_of_SingleTop_background_one_reco;
  TH1F *ljet_el_mass_of_diboson_background_one_reco;
  TH1F *ljet_el_mass_of_Z_jets_background_one_reco;
  TH1F *ljet_el_mass_of_ttbar_background_one_reco;
  TH1F *ljet_el_mass_of_W_jets_background_one_reco;
  TH1F *ljet_el_mass_of_dijet_background_one_reco;
  TH1F *ljet_el_mass_of_signal_one_reco;
  TH1F *ljet_el_mass_of_physicsdata_one_reco;
  std::vector<TH1F*> *ljet_el_mass_background_all_one_reco = new std::vector<TH1F*>();

  float content_bkg_all_one_reco;
  float content_sig_all_one_reco;
  float content_data_all_one_reco;

  float error_bkg_all_one_reco;
  float error_sig_all_one_reco;
  float error_data_all_one_reco;

 private:
  std::vector<float> content_sig_all_vector_one_reco;
  std::vector<float> content_bkg_all_vector_one_reco;
  std::vector<float> content_data_all_vector_one_reco;

  std::vector<float> error_sig_all_vector_one_reco;
  std::vector<float> error_bkg_all_vector_one_reco;
  std::vector<float> error_data_all_vector_one_reco;

  std::vector<float> pvalue_vector_one_reco;
  std::vector<float> zvalue_vector_one_reco;

 public:
  // For 2el + 1lj channel
  void Reset_ALL_two_reco();
  void Read_from_file_BKG_two_reco(TString input_file_bkg);
  void Read_from_file_SIG_two_reco(TString input_file_sig);
  void Read_from_file_DATA_two_reco(TString input_file_data);
  void CheckContent_BKG_two_reco(float integral_lumi_origin, float integral_lumi_ref);
  void CheckContent_SIG_two_reco(float integral_lumi_origin, float integral_lumi_ref);
  void CheckContent_DATA_two_reco();
  void Calculate_ExpPZ_two_reco();

 private:
  TH1F *ljet_el_mass_of_SingleTop_background_two_reco;
  TH1F *ljet_el_mass_of_diboson_background_two_reco;
  TH1F *ljet_el_mass_of_Z_jets_background_two_reco;
  TH1F *ljet_el_mass_of_ttbar_background_two_reco;
  TH1F *ljet_el_mass_of_W_jets_background_two_reco;
  TH1F *ljet_el_mass_of_dijet_background_two_reco;
  TH1F *ljet_el_mass_of_signal_two_reco;
  TH1F *ljet_el_mass_of_physicsdata_two_reco;
  std::vector<TH1F*> *ljet_el_mass_background_all_two_reco = new std::vector<TH1F*>();
  float content_bkg_all_two_reco;
  float content_sig_all_two_reco;
  float content_data_all_two_reco;

 private:
  std::vector<float> content_sig_all_vector_two_reco;
  std::vector<float> content_bkg_all_vector_two_reco;
  std::vector<float> content_data_all_vector_two_reco;
  std::vector<float> pvalue_vector_two_reco;
  std::vector<float> zvalue_vector_two_reco;

 public:
  // Common tools
  void Figure_Prepare(TString outputfilename);
  std::vector<unsigned int> Figure_Sort_With_HistContent(std::vector<TH1F*> *hist_vector);
  void Figure_Prepare_ConfigText();
  void Figure_MakeLegend_BKG_SIG(TLegend *legend);
  void Figure_MakeLegend_BKG_SIG_DATA(TLegend *legend);
  void Figure_CanvasSetting_BKG_SIG_Without_RatioPlot(TCanvas *c, THStack *stack_background, TH1F *hist_signal, TLegend *legend,
                                                 bool fixed_yaxis, float yaxis_min, float yaxis_max, bool yaxis_log);
  void Figure_CanvasSetting_BKG_SIG(TCanvas *c, THStack *stack_background, TH1F *hist_signal, TLegend *legend,
                                    bool fixed_yaxis, float yaxis_min, float yaxis_max, bool yaxis_log);
  void Figure_CanvasSetting_BKG_SIG_DATA(TCanvas *c, THStack *stack_background, TH1F *hist_signal, TH1F *hist_data, TLegend *legend,
                                         bool fixed_yaxis, float yaxis_min, float yaxis_max, bool yaxis_log);
  void Figure_CreateUserDefinedCanvas_Margin(TCanvas *c);
  void Figure_CreateUserDefinedCanvas_BKG_SIG(TCanvas *c);
  void Figure_CreateUserDefinedCanvas_BKG_SIG_DATA(TCanvas *c);
  void Figure_Close();

 private:
  TFile *fileout;

 private:
  float ATLASLabel_xposition = 0.20;
  float ATLASLabel_yposition = 0.82;

  // For 1el + 1lj channel
 public:
  void Figure_Combine_BKG_SIG_one_reco();
  void Figure_Combine_BKG_SIG_DATA_one_reco();
  void Figure_Efficiencies_BKG_SIG_one_reco();

  // For 2el + 1lj channel
 public:
  void Figure_Combine_BKG_SIG_two_reco();
  void Figure_Combine_BKG_SIG_DATA_two_reco();
  void Figure_Efficiencies_BKG_SIG_two_reco();

};

#endif

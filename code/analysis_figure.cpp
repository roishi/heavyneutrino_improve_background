#ifndef HEAVYNEUTRINO_IMPROVE_BACKGROUND_ANALYSIS_FIGURE_CPP
#define HEAVYNEUTRINO_IMPROVE_BACKGROUND_ANALYSIS_FIGURE_CPP

void analysis::Figure_Prepare(TString outputfilename){
  fileout = new TFile(outputfilename, "RECREATE");
}

std::vector<unsigned int> analysis::Figure_Sort_With_HistContent(std::vector<TH1F*> *hist_vector){
  std::vector<float> content_all_vector;
  for (unsigned int index = 0; index < hist_vector->size(); index++){
    unsigned int nbins = hist_vector->at(index)->GetXaxis()->GetNbins();
    float content_all = 0;
    for (unsigned int ibin = 1; ibin < nbins + 1; ibin++){
      float content = hist_vector->at(index)->GetBinContent(ibin);
      content_all = content_all + content;
    }
    content_all_vector.push_back(content_all);
  }

  std::vector<unsigned int> content_all_vector_index;
  for (unsigned int index = 0; index < content_all_vector.size(); index++){
    content_all_vector_index.push_back(static_cast<std::size_t>(index));
  }

  std::sort(content_all_vector_index.begin(), content_all_vector_index.end(),
           [content_all_vector](std::size_t lhs = 0, std::size_t rhs = 0){
              return content_all_vector.at(lhs) < content_all_vector.at(rhs);
            });

  return content_all_vector_index;
}

void analysis::Figure_Prepare_ConfigText(){
  TLatex latex;
  TString latex_string;
  float xposition = 0.200;
  float yposition = 0.800;
  float yspan = 0.032;
  latex.SetTextSize(0.03);
  latex.SetTextAlign(13);
  latex_string = "#sqrt{s} = 13 TeV, 139 fb^{-1}";
  latex.DrawLatexNDC(xposition, yposition, latex_string);
  yposition = yposition - yspan - 0.005;
  latex_string = "Leading electron isolation: " + config_Isolation;
  latex.DrawLatexNDC(xposition, yposition, latex_string);
  yposition = yposition - yspan;
  latex_string = "Leading electron Quality: " + config_Quality;
  latex.DrawLatexNDC(xposition, yposition, latex_string);
  yposition = yposition - yspan;
  latex_string.Form("Leading electron pt: %.2f GeV", config_leading_electron_pt_cut);
  latex.DrawLatexNDC(xposition, yposition, latex_string);
  yposition = yposition - yspan;
  latex_string.Form("Number of fatjet: %d, fatjet mass: %.1f", config_number_of_fatjet, config_fatjet_mass);
  latex.DrawLatexNDC(xposition, yposition, latex_string);
  yposition = yposition - yspan;
  latex_string.Form("MET: %.2f GeV, #Delta#eta_{lj,e1}: %.1f", config_MET_cut, config_delta_eta_cut);
  latex.DrawLatexNDC(xposition, yposition, latex_string);
  yposition = yposition - yspan;
  latex_string.Form("N Signal: %.2f, N Background: %.2f, P-value: %.2f",
                    content_sig_all_vector_one_reco.at(content_sig_all_vector_one_reco.size()-1),
                    content_bkg_all_vector_one_reco.at(content_bkg_all_vector_one_reco.size()-1),
                    pvalue_vector_one_reco.at(pvalue_vector_one_reco.size()-1));
  latex.DrawLatexNDC(xposition, yposition, latex_string);
  yposition = yposition - yspan;
}
void analysis::Figure_MakeLegend_BKG_SIG(TLegend *legend){
  legend->SetBorderSize(0);
  legend->SetTextSize(0.03);
  legend->AddEntry(ljet_el_mass_of_SingleTop_background_one_reco, "SingleTop", "f");
  legend->AddEntry(ljet_el_mass_of_diboson_background_one_reco, "diboson", "f");
  legend->AddEntry(ljet_el_mass_of_Z_jets_background_one_reco, "Z+jets", "f");
  legend->AddEntry(ljet_el_mass_of_ttbar_background_one_reco, "ttbar", "f");
  legend->AddEntry(ljet_el_mass_of_W_jets_background_one_reco, "W+jets", "f");
  legend->AddEntry(ljet_el_mass_of_dijet_background_one_reco, "dijet", "f");
  legend->AddEntry(ljet_el_mass_of_signal_one_reco, "signal", "l");
}

void analysis::Figure_MakeLegend_BKG_SIG_DATA(TLegend *legend){
  legend->SetBorderSize(0);
  legend->SetTextSize(0.03);
  legend->AddEntry(ljet_el_mass_of_SingleTop_background_one_reco, "SingleTop", "f");
  legend->AddEntry(ljet_el_mass_of_diboson_background_one_reco, "diboson", "f");
  legend->AddEntry(ljet_el_mass_of_Z_jets_background_one_reco, "Z+jets", "f");
  legend->AddEntry(ljet_el_mass_of_ttbar_background_one_reco, "ttbar", "f");
  legend->AddEntry(ljet_el_mass_of_W_jets_background_one_reco, "W+jets", "f");
  legend->AddEntry(ljet_el_mass_of_dijet_background_one_reco, "dijet", "f");
  legend->AddEntry(ljet_el_mass_of_signal_one_reco, "signal", "l");
  legend->AddEntry(ljet_el_mass_of_physicsdata_one_reco, "data", "p");
}

void analysis::Figure_CanvasSetting_BKG_SIG_Without_RatioPlot(TCanvas *c, THStack *stack_background, TH1F *hist_signal, TLegend *legend,
                                                              bool fixed_yaxis, float yaxis_min, float yaxis_max, bool yaxis_log){
  hist_signal->SetLineColor(kRed); //Red
  hist_signal->SetLineWidth(2);
  TLine *line = new TLine(signal_region_mass_min, yaxis_min*1e-1, signal_region_mass_min, yaxis_max*1e1);
  line->SetLineWidth(4);
  line->SetLineColor(kRed);
  Figure_CreateUserDefinedCanvas_Margin(c);
  c->cd();
  stack_background->Draw("hist");
  hist_signal->SetFillStyle(4050);
  hist_signal->Draw("E same");
  legend->Draw("same");
  Figure_Prepare_ConfigText();
  if (fixed_yaxis) stack_background->SetMinimum(yaxis_min);
  if (fixed_yaxis) stack_background->SetMaximum(yaxis_max);
  ATLASLabel(ATLASLabel_xposition, ATLASLabel_yposition, "Simulation Work in Progress");
  gPad->SetLogy(yaxis_log);
  gPad->SetTickx(1);
  gPad->SetTicky(1);
  fileout->cd();
  c->Write();
}

void analysis::Figure_CanvasSetting_BKG_SIG(TCanvas *c, THStack *stack_background, TH1F *hist_signal, TLegend *legend,
                                            bool fixed_yaxis, float yaxis_min, float yaxis_max, bool yaxis_log){
  hist_signal->SetLineColor(kRed); //Red
  hist_signal->SetLineWidth(2);
  TLine *line = new TLine(signal_region_mass_min, yaxis_min*1e-1, signal_region_mass_min, yaxis_max*1e1);
  line->SetLineWidth(4);
  line->SetLineColor(kRed);
  Figure_CreateUserDefinedCanvas_BKG_SIG(c);
  // First pad to plot combine plots
  c->cd(1);
  stack_background->Draw("hist");
  hist_signal->SetFillStyle(4050);
  hist_signal->Draw("E same"); // E1 -> Draw error bar with perpendicular lines, E2 -> Draw error bar with rectangles
  // line->Draw("same");
  legend->Draw("same");
  Figure_Prepare_ConfigText();
  if (fixed_yaxis) stack_background->SetMinimum(yaxis_min);
  if (fixed_yaxis) stack_background->SetMaximum(yaxis_max);
  ATLASLabel(ATLASLabel_xposition, ATLASLabel_yposition, "Simulation Work in Progress");
  gPad->SetLogy(yaxis_log);
  gPad->SetTickx(1);
  gPad->SetTicky(1);
  // Second pad to plot signal / background
  TH1F *last_stack_hist = (TH1F*)stack_background->GetStack()->Last();
  unsigned int nbin = last_stack_hist->GetXaxis()->GetNbins();
  TLine *line_ratio_one = new TLine(last_stack_hist->GetXaxis()->GetXmin(), 1, last_stack_hist->GetXaxis()->GetXmax(), 1);
  line_ratio_one->SetLineWidth(1);
  line_ratio_one->SetLineColor(kRed);
  c->cd(2);
  TString axis_titles;
  axis_titles.Form(";%s;signal / background ", hist_signal->GetXaxis()->GetTitle());
  TH1F* ratio_signal_background_hist = new TH1F("", axis_titles, nbin, last_stack_hist->GetXaxis()->GetXmin(), last_stack_hist->GetXaxis()->GetXmax());
  ratio_signal_background_hist->Sumw2();
  ratio_signal_background_hist->SetStats(kFALSE);
  ratio_signal_background_hist->SetLabelSize(8/3 * stack_background->GetXaxis()->GetLabelSize(), "xy");
  ratio_signal_background_hist->SetTitleSize(8/3 * stack_background->GetXaxis()->GetTitleSize(), "xy");
  ratio_signal_background_hist->GetYaxis()->SetTitleOffset(0.6 * stack_background->GetXaxis()->GetTitleOffset());
  ratio_signal_background_hist->SetTitleOffset(stack_background->GetXaxis()->GetTitleOffset());
  ratio_signal_background_hist->GetYaxis()->SetRangeUser(5e-3, 5e2);
  for (unsigned int ibin = 1; ibin < nbin+1; ibin++){
    float bincontent = 0;
    float binerror = 0;
    if (last_stack_hist->GetBinContent(ibin) <= 0)
      continue;
    if (hist_signal->GetBinContent(ibin) <= 0)
      continue;
    bincontent = hist_signal->GetBinContent(ibin) / last_stack_hist->GetBinContent(ibin);
    binerror = ErrorPropagation_division(hist_signal->GetBinContent(ibin), last_stack_hist->GetBinContent(ibin),
                                         hist_signal->GetBinError(ibin), last_stack_hist->GetBinError(ibin));
    ratio_signal_background_hist->SetBinContent(ibin, bincontent);
    ratio_signal_background_hist->SetBinError(ibin, binerror);
  }
  ratio_signal_background_hist->SetMarkerStyle(20);
  ratio_signal_background_hist->Draw("p");
  line_ratio_one->Draw("same");
  gPad->SetLogy(true);
  gPad->SetTickx(1);
  gPad->SetTicky(1);
  fileout->cd();
  c->Write();
}

void analysis::Figure_CanvasSetting_BKG_SIG_DATA(TCanvas *c, THStack *stack_background, TH1F *hist_signal, TH1F *hist_data, TLegend *legend,
                                                 bool fixed_yaxis, float yaxis_min, float yaxis_max, bool yaxis_log){
  hist_signal->SetLineColor(kRed); // Red
  hist_data->SetMarkerStyle(20);
  TLine *line = new TLine(signal_region_mass_min, yaxis_min*1e-1, signal_region_mass_min, yaxis_max*1e1);
  line->SetLineWidth(4);
  line->SetLineColor(kRed);
  Figure_CreateUserDefinedCanvas_BKG_SIG_DATA(c); // Create proper canvas
  // First pad to plot combine plots
  c->cd(1);
  stack_background->Draw("hist");
  hist_signal->Draw("same");
  hist_data->Draw("same");
  // line->Draw("same");
  legend->Draw("same");
  Figure_Prepare_ConfigText();
  if (fixed_yaxis) stack_background->SetMinimum(yaxis_min);
  if (fixed_yaxis) stack_background->SetMaximum(yaxis_max);
  ATLASLabel(ATLASLabel_xposition, ATLASLabel_yposition, "Work in progress");
  gPad->SetLogy(yaxis_log);
  gPad->SetTickx(1);
  gPad->SetTicky(1);
  // Second pad to plot data / background
  TH1F* last_stack_hist = (TH1F*)stack_background->GetStack()->Last();
  unsigned int nbin = last_stack_hist->GetXaxis()->GetNbins();
  c->cd(2);
  TLine *line_ratio_one = new TLine(last_stack_hist->GetXaxis()->GetXmin(), 1, last_stack_hist->GetXaxis()->GetXmax(), 1);
  line_ratio_one->SetLineWidth(1);
  line_ratio_one->SetLineColor(kRed);
  TH1F* ratio_data_background_hist = new TH1F("", ";;data / background", nbin, last_stack_hist->GetXaxis()->GetXmin(), last_stack_hist->GetXaxis()->GetXmax());
  ratio_data_background_hist->Sumw2();
  ratio_data_background_hist->SetStats(kFALSE);
  ratio_data_background_hist->SetLabelSize(0.1, "x");
  ratio_data_background_hist->SetLabelSize(0.1, "y");
  ratio_data_background_hist->SetTitleSize(0.1, "y");
  ratio_data_background_hist->GetYaxis()->SetTitleOffset(0.45);
  ratio_data_background_hist->GetYaxis()->SetRangeUser(0.0, 2.0);
  for (unsigned int ibin = 1; ibin < nbin+1; ibin++){
    float bincontent = 0;
    float binerror = 0;
    if (last_stack_hist->GetBinContent(ibin) <= 0)
      continue;
    if (hist_data->GetBinContent(ibin) <= 0)
      continue;
    bincontent = hist_data->GetBinContent(ibin) / last_stack_hist->GetBinContent(ibin);
    binerror = ErrorPropagation_division(hist_data->GetBinContent(ibin), last_stack_hist->GetBinContent(ibin),
                                         hist_data->GetBinError(ibin), last_stack_hist->GetBinError(ibin));
    ratio_data_background_hist->SetBinContent(ibin, bincontent);
    ratio_data_background_hist->SetBinError(ibin, binerror);
  }
  ratio_data_background_hist->SetMarkerStyle(20);
  ratio_data_background_hist->Draw("p");
  line_ratio_one->Draw("same");
  gPad->SetTickx(1);
  gPad->SetTicky(1);
  // Third pad to plot signal / background
  c->cd(3);
  TString axis_titles;
  axis_titles.Form(";%s;signal / background", hist_signal->GetXaxis()->GetTitle());
  TH1F* ratio_signal_background_hist = new TH1F("", axis_titles, nbin, last_stack_hist->GetXaxis()->GetXmin(), last_stack_hist->GetXaxis()->GetXmax());
  ratio_signal_background_hist->Sumw2();
  ratio_signal_background_hist->SetStats(kFALSE);
  ratio_signal_background_hist->SetLabelSize(0.1, "x");
  ratio_signal_background_hist->SetLabelSize(0.1, "y");
  ratio_signal_background_hist->SetTitleSize(0.1, "x");
  ratio_signal_background_hist->SetTitleSize(0.1, "y");
  ratio_signal_background_hist->GetYaxis()->SetTitleOffset(0.45);
  ratio_signal_background_hist->GetYaxis()->SetRangeUser(1e-5, 1e3);
  for (unsigned int ibin = 1; ibin < nbin+1; ibin++){
    float bincontent = 0;
    float binerror = 0;
    if (last_stack_hist->GetBinContent(ibin) <= 0){
      continue;
    }
    if (hist_signal->GetBinContent(ibin) <= 0){
      continue;
    }
    bincontent = hist_signal->GetBinContent(ibin) / last_stack_hist->GetBinContent(ibin);
    binerror = ErrorPropagation_division(hist_signal->GetBinContent(ibin), last_stack_hist->GetBinContent(ibin),
                                         hist_signal->GetBinError(ibin), last_stack_hist->GetBinError(ibin));
    ratio_signal_background_hist->SetBinContent(ibin, bincontent);
    ratio_signal_background_hist->SetBinError(ibin, binerror);
  }
  ratio_signal_background_hist->SetMarkerStyle(20);
  ratio_signal_background_hist->Draw("p");
  line_ratio_one->Draw("same");
  gPad->SetLogy(true);
  gPad->SetTickx(1);
  gPad->SetTicky(1);
  fileout->cd();
  c->Write();
}

void analysis::Figure_CreateUserDefinedCanvas_Margin(TCanvas *c){
  c->Draw();
  c->SetLeftMargin(0.15);
  c->SetBottomMargin(0.15);
}

void analysis::Figure_CreateUserDefinedCanvas_BKG_SIG(TCanvas *c){
  c->Draw();
  TPad *pad_combine = new TPad("", "", 0.0, 0.3, 1.0, 1.0);
  pad_combine->SetBottomMargin(0);
  pad_combine->SetLeftMargin(0.15);
  pad_combine->Draw();
  pad_combine->SetNumber(1);

  TPad *pad_sig_bkg = new TPad("", "", 0.0, 0.0, 1.0, 0.3);
  pad_sig_bkg->SetTopMargin(0);
  pad_sig_bkg->SetLeftMargin(0.15);
  pad_sig_bkg->SetBottomMargin(0.30);
  pad_sig_bkg->Draw();
  pad_sig_bkg->SetNumber(2);
}

void analysis::Figure_CreateUserDefinedCanvas_BKG_SIG_DATA(TCanvas *c){
  c->Draw();
  TPad *pad_combine = new TPad("", "", 0.0, 0.4, 1.0, 1.0);
  pad_combine->SetBottomMargin(0);
  pad_combine->Draw();
  pad_combine->SetNumber(1);

  c->cd();
  TPad *pad_data_bkg = new TPad("", "", 0.0, 0.2, 1.0, 0.4);
  pad_data_bkg->SetTopMargin(0);
  pad_data_bkg->SetBottomMargin(0);
  pad_data_bkg->Draw();
  pad_data_bkg->SetNumber(2);

  c->cd();
  TPad *pad_sig_bkg = new TPad("", "", 0.0, 0.0, 1.0, 0.2);
  pad_sig_bkg->SetTopMargin(0);
  pad_sig_bkg->Draw();
  pad_sig_bkg->SetNumber(3);
}

void analysis::Figure_Close(){
  fileout->Close();
}

#endif

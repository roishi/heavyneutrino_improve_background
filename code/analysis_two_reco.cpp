#ifndef HEAVYNEUTRINO_IMPROVE_BACKGROUND_ANALYSIS_TWO_RECO_CPP
#define HEAVYNEUTRINO_IMPROVE_BACKGROUND_ANALYSIS_TWO_RECO_CPP

#include "analysis.h"

// There are only functions for 2el + 1lj channel
void analysis::Reset_ALL_two_reco(){
  file_bkg_two_reco->Close();
  file_sig_two_reco->Close();
  file_data_two_reco->Close();

  content_bkg_all_two_reco = 0;
  content_sig_all_two_reco = 0;
  content_data_all_two_reco = 0;
  ljet_el_mass_of_SingleTop_background_two_reco = nullptr;
  ljet_el_mass_of_diboson_background_two_reco = nullptr;
  ljet_el_mass_of_Z_jets_background_two_reco = nullptr;
  ljet_el_mass_of_ttbar_background_two_reco = nullptr;
  ljet_el_mass_of_W_jets_background_two_reco = nullptr;
  ljet_el_mass_of_dijet_background_two_reco = nullptr;
  ljet_el_mass_background_all_two_reco->clear();
}

void analysis::Read_from_file_BKG_two_reco(TString input_file_bkg){
  TFile *file_bkg_two_reco = TFile::Open(input_file_bkg);
  TString image_name;
  image_name = "ljet_el_mass_of_SingleTop_two_reco";
  ljet_el_mass_of_SingleTop_background_two_reco = (TH1F*)file_bkg_two_reco->Get(image_name);
  ljet_el_mass_background_all_two_reco->push_back(ljet_el_mass_of_SingleTop_background_two_reco);
  image_name = "ljet_el_mass_of_diboson_two_reco";
  ljet_el_mass_of_diboson_background_two_reco = (TH1F*)file_bkg_two_reco->Get(image_name);
  ljet_el_mass_background_all_two_reco->push_back(ljet_el_mass_of_diboson_background_two_reco);
  image_name = "ljet_el_mass_of_Z+jets_two_reco";
  ljet_el_mass_of_Z_jets_background_two_reco = (TH1F*)file_bkg_two_reco->Get(image_name);
  ljet_el_mass_background_all_two_reco->push_back(ljet_el_mass_of_Z_jets_background_two_reco);
  image_name = "ljet_el_mass_of_ttbar_two_reco";
  ljet_el_mass_of_ttbar_background_two_reco = (TH1F*)file_bkg_two_reco->Get(image_name);
  ljet_el_mass_background_all_two_reco->push_back(ljet_el_mass_of_ttbar_background_two_reco);
  image_name = "ljet_el_mass_of_W+jets_two_reco";
  ljet_el_mass_of_W_jets_background_two_reco = (TH1F*)file_bkg_two_reco->Get(image_name);
  ljet_el_mass_background_all_two_reco->push_back(ljet_el_mass_of_W_jets_background_two_reco);
  image_name = "ljet_el_mass_of_dijet_two_reco";
  ljet_el_mass_of_dijet_background_two_reco = (TH1F*)file_bkg_two_reco->Get(image_name);
  // ljet_el_mass_background_all_two_reco->push_back(ljet_el_mass_of_dijet_background_two_reco);

  if ((ljet_el_mass_of_SingleTop_background_two_reco == nullptr) || (ljet_el_mass_of_diboson_background_two_reco == nullptr) ||
      (ljet_el_mass_of_Z_jets_background_two_reco == nullptr) || (ljet_el_mass_of_ttbar_background_two_reco == nullptr) ||
      (ljet_el_mass_of_W_jets_background_two_reco == nullptr) || (ljet_el_mass_of_dijet_background_two_reco == nullptr)){
    std::cout << "At least one background process have not ljet-el mass plot (2 electron + 1 learge-R jet channel).\nAbort!!" << std::endl;
    exit(1);
  }
}

void analysis::Read_from_file_SIG_two_reco(TString input_file_sig){
  TFile *file_sig_two_reco = TFile::Open(input_file_sig);
  TString image_name;
  image_name = "ljet_el_mass_of_5TeV-200GeV_two_reco"; // The file you specified include 5TeV-200GeV images ?
  if (file_sig_two_reco->GetListOfKeys()->Contains(image_name))
    ljet_el_mass_of_signal_two_reco = (TH1F*)file_sig_two_reco->Get(image_name);
  image_name = "ljet_el_mass_of_5TeV-500GeV_two_reco"; // The file you specified include 5TeV-500GeV images ?
  if (file_sig_two_reco->GetListOfKeys()->Contains(image_name))
    ljet_el_mass_of_signal_two_reco = (TH1F*)file_sig_two_reco->Get(image_name);

  if (ljet_el_mass_of_signal_two_reco == nullptr){
    std::cout << "The file for signal sample has no ljet-el mass plot.\nAbort!!" << std::endl;
    exit(1);
  }
}

void analysis::Read_from_file_DATA_two_reco(TString input_file_data){
  TFile *file_data_two_reco = TFile::Open(input_file_data);
  TString image_name;
  image_name = "ljet_el_mass_of_physicsdata_two_reco";
  ljet_el_mass_of_physicsdata_two_reco = (TH1F*)file_data_two_reco->Get(image_name);

  if (ljet_el_mass_of_physicsdata_two_reco == nullptr){
    std::cout << "The file for physics data has no ljet-el mass plot.\nAbort!!" << std::endl;
    exit(1);
  }
}

void analysis::CheckContent_BKG_two_reco(float integral_lumi_origin, float integral_lumi_ref){
  unsigned int nbin = ljet_el_mass_of_signal_two_reco->GetXaxis()->GetNbins();
  float xmin = ljet_el_mass_of_signal_two_reco->GetXaxis()->GetXmin();
  float xmax = ljet_el_mass_of_signal_two_reco->GetXaxis()->GetXmax();
  float xspan = (xmax - xmin) / nbin;

  content_bkg_all_two_reco = 0; // initialize
  for (unsigned int ibin = 1; ibin < nbin+2; ibin++){
    if (xspan * (ibin-1) >= signal_region_mass_min){
      for (unsigned int hist_index = 0; hist_index < ljet_el_mass_background_all_two_reco->size(); hist_index++){
        content_bkg_all_two_reco = content_bkg_all_two_reco + ljet_el_mass_background_all_two_reco->at(hist_index)->GetBinContent(ibin);
      }
    }
  }
  content_bkg_all_two_reco = content_bkg_all_two_reco * (integral_lumi_ref / integral_lumi_origin);
  content_bkg_all_vector_two_reco.push_back(content_bkg_all_two_reco);
}

void analysis::CheckContent_SIG_two_reco(float integral_lumi_origin, float integral_lumi_ref){
  unsigned int nbin = ljet_el_mass_of_signal_two_reco->GetXaxis()->GetNbins();
  float xmin = ljet_el_mass_of_signal_two_reco->GetXaxis()->GetXmin();
  float xmax = ljet_el_mass_of_signal_two_reco->GetXaxis()->GetXmax();
  float xspan = (xmax - xmin) / nbin;

  content_sig_all_two_reco = 0; // initialize
  for (unsigned int ibin = 1; ibin < nbin+2; ibin++){
    if (xspan * (ibin-1) >= signal_region_mass_min){
      content_sig_all_two_reco = content_sig_all_two_reco + ljet_el_mass_of_signal_two_reco->GetBinContent(ibin);
    }
  }
  content_sig_all_two_reco = content_sig_all_two_reco * (integral_lumi_ref / integral_lumi_origin);
  content_sig_all_vector_two_reco.push_back(content_sig_all_two_reco);
}

void analysis::CheckContent_DATA_two_reco(){
  unsigned int nbin = ljet_el_mass_of_signal_two_reco->GetXaxis()->GetNbins();
  float xmin = ljet_el_mass_of_signal_two_reco->GetXaxis()->GetXmin();
  float xmax = ljet_el_mass_of_signal_two_reco->GetXaxis()->GetXmax();
  float xspan = (xmax - xmin) / nbin;

  content_data_all_two_reco = 0; // initialize
  for (unsigned int ibin = 1; ibin < nbin+2; ibin++){
    if (xspan * (ibin-1) >= signal_region_mass_min){
      content_data_all_two_reco = content_data_all_two_reco + ljet_el_mass_of_physicsdata_two_reco->GetBinContent(ibin);
    }
  }
  content_data_all_vector_two_reco.push_back(content_data_all_two_reco);
}

void analysis::Calculate_ExpPZ_two_reco(){
  float n_signal = content_sig_all_vector_two_reco.at(content_sig_all_vector_two_reco.size()-1);
  float n_background = content_bkg_all_vector_two_reco.at(content_bkg_all_vector_two_reco.size()-1);
  double pExp = Get_Pvalue(n_signal, n_background);
  double zExp = Get_Zvalue(n_signal, n_background);
  pvalue_vector_two_reco.push_back(pExp);
  zvalue_vector_two_reco.push_back(zExp);
}

#endif

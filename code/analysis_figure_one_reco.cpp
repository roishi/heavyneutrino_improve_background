#ifndef HEAVYNEUTRINO_IMPROVE_BACKGROUND_ANALYSIS_FIGURE_ONE_RECO_CPP
#define HEAVYNEUTRINO_IMPROVE_BACKGROUND_ANALYSIS_FIGURE_ONE_RECO_CPP

#include "analysis.h"

void analysis::Figure_Combine_BKG_SIG_one_reco(){
  std::vector<unsigned int> ordered_indices = Figure_Sort_With_HistContent(ljet_el_mass_background_all_one_reco);
  TString thstack_title;
  thstack_title.Form("%s;%s;%s", ljet_el_mass_of_signal_one_reco->GetTitle(), ljet_el_mass_of_signal_one_reco->GetXaxis()->GetTitle(), ljet_el_mass_of_signal_one_reco->GetYaxis()->GetTitle());
  THStack *stack_hist = new THStack(ljet_el_mass_of_signal_one_reco->GetName(), thstack_title);
  for (unsigned int index = 0; index < ordered_indices.size(); index++){
    unsigned int ordered_index = ordered_indices.at(index);
    stack_hist->Add(ljet_el_mass_background_all_one_reco->at(ordered_index));
  }

  TCanvas *c = new TCanvas(configall_vector.at(configall_vector.size()-1) + "_one_reco",
                           configall_vector.at(configall_vector.size()-1) + "_one_reco",
                           700, 600);
  float legend_ymax = 0.88;
  float legend_size = 0.05 * 7;
  TLegend *legend = new TLegend(0.7, legend_ymax - legend_size, 0.88, legend_ymax);
  Figure_MakeLegend_BKG_SIG(legend);
  bool fixed_yaxis = true;
  float yaxis_min = 1e-2;
  float yaxis_max = 1e6;
  bool yaxis_log = true;
  Figure_CanvasSetting_BKG_SIG(c, stack_hist, ljet_el_mass_of_signal_one_reco, legend, fixed_yaxis, yaxis_min, yaxis_max, yaxis_log);
  // Figure_CanvasSetting_BKG_SIG_Without_RatioPlot(c, stack_hist, ljet_el_mass_of_signal_one_reco, legend, fixed_yaxis, yaxis_min, yaxis_max, yaxis_log);
}

void analysis::Figure_Combine_BKG_SIG_DATA_one_reco(){
  std::vector<unsigned int> ordered_indices = Figure_Sort_With_HistContent(ljet_el_mass_background_all_one_reco);
  TString thstack_title;
  thstack_title.Form("%s;%s;%s", ljet_el_mass_of_signal_one_reco->GetTitle(), ljet_el_mass_of_signal_one_reco->GetXaxis()->GetTitle(), ljet_el_mass_of_signal_one_reco->GetYaxis()->GetTitle());
  THStack *stack_hist = new THStack(ljet_el_mass_of_signal_one_reco->GetName(), thstack_title);
  for (unsigned int index = 0; index < ordered_indices.size(); index++){
    unsigned int ordered_index = ordered_indices.at(index);
    stack_hist->Add(ljet_el_mass_background_all_one_reco->at(ordered_index));
  }

  TCanvas *c = new TCanvas(configall_vector.at(configall_vector.size()-1) + "_one_reco",
                           configall_vector.at(configall_vector.size()-1) + "_one_reco",
                           700, 600);
  float legend_ymax = 0.88;
  float legend_size = 0.05 * 8;
  TLegend *legend = new TLegend(0.7, legend_ymax - legend_size, 0.88, legend_ymax);
  Figure_MakeLegend_BKG_SIG_DATA(legend);
  bool fixed_yaxis = true;
  float yaxis_min = 1e-2;
  float yaxis_max = 1e8;
  bool yaxis_log = true;
  Figure_CanvasSetting_BKG_SIG_DATA(c, stack_hist, ljet_el_mass_of_signal_one_reco, ljet_el_mass_of_physicsdata_one_reco, legend, fixed_yaxis, yaxis_min, yaxis_max, yaxis_log);
}

void analysis::Figure_Efficiencies_BKG_SIG_one_reco(){
  float signal_efficiency_min = 0;
  float signal_efficiency_max = 1;
  float signal_efficiency_resolution = 0.001;
  unsigned int n_trial_signal = (unsigned int)((signal_efficiency_max - signal_efficiency_min) / signal_efficiency_resolution);

  float background_efficiency_min = 0;
  float background_efficiency_max = 1;
  float background_efficiency_resolution = 0.001;
  unsigned int n_trial_background = (unsigned int)((background_efficiency_max - background_efficiency_min) / background_efficiency_resolution);

  TH2D *hist = new TH2D(configall_vector.at(configall_vector.size()-1) + "_Z-value_one_reco",
                        configall_vector.at(configall_vector.size()-1) + "_Z-value_one_reco;signal efficiency;background efficiency",
                        n_trial_signal, signal_efficiency_min, signal_efficiency_max,
                        n_trial_background, background_efficiency_min, background_efficiency_max);
  hist->SetStats(kFALSE);

  for (unsigned int i_trial_signal = 0; i_trial_signal < n_trial_signal+1; i_trial_signal++){
    for (unsigned int i_trial_background = 0; i_trial_background < n_trial_background+1; i_trial_background++){
      if (signal_efficiency_min + signal_efficiency_resolution * i_trial_signal > background_efficiency_min + background_efficiency_resolution * i_trial_background){
        float number_of_signal_times_efficiency = content_sig_all_vector_one_reco.at(content_sig_all_vector_one_reco.size()-1) * (signal_efficiency_min + signal_efficiency_resolution * i_trial_signal);
        float number_of_background_times_efficiency = content_bkg_all_vector_one_reco.at(content_bkg_all_vector_one_reco.size()-1) * (background_efficiency_min + background_efficiency_resolution * i_trial_background);
        double z = RooStats::NumberCountingUtils::BinomialExpZ(number_of_signal_times_efficiency, number_of_background_times_efficiency, 0.5);
        if (z > 0){
          hist->Fill(signal_efficiency_min + signal_efficiency_resolution * i_trial_signal,
                     background_efficiency_min + background_efficiency_resolution * i_trial_background,
                     z);
        }
      }
    }
  }

  double contours[2];
  contours[0] = 3;
  contours[1] = 5;

  TF1 *f = new TF1("function_zero_zero_to_one_one", "x", 0, 1);
  f->SetLineColor(kGreen);
  f->SetLineWidth(2);

  TGraph *gr = new TGraph("../../heavyneutrino_ml_update/withcalocell/tpr_fpr_points.dat", "%lg %lg", ",");
  gr->SetLineColor(kBlack);
  gr->SetLineWidth(2);

  TCanvas *c_hist = new TCanvas("Z-values_one_reco_" + configall_vector.at(configall_vector.size()-1),
                                "Z-values_one_reco_" + configall_vector.at(configall_vector.size()-1));
  c_hist->cd();
  hist->DrawCopy("colz");
  hist->SetContour(2, contours);
  hist->Draw("cont3 same");
  hist->SetLineWidth(2);
  hist->SetLineColor(kRed);
  f->Draw("same");
  gr->Draw("same");
  Figure_Prepare_ConfigText();
  fileout->cd();
  gPad->SetLogy(1);
  ATLASLabel(0.15, 0.82, "Work in progress");
  gPad->SetTickx(1);
  gPad->SetTicky(1);
  fileout->cd();
  c_hist->Write();

  delete f;
  delete gr;
  delete c_hist;
}

#endif

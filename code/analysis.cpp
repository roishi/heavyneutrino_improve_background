// Todo
// 1. Need to find a method to know the mass point of signal sample

#include "analysis.h"
#include "analysis_one_reco.cpp"
#include "analysis_two_reco.cpp"
#include "analysis_figure.cpp"
#include "analysis_figure_one_reco.cpp"
#include "analysis_figure_two_reco.cpp"

void analysis::Append_Configall(TString configall){
  configall_vector.push_back(configall);
}

void analysis::UnSetConfigSetting(){
  config_MET_cut = 0;
  config_METsignificant_cut = 0;
  config_Isolation = "";
  config_Quality = "";
  config_fatjet_mass_upper_cut = 0;
  config_leading_electron_pt_cut = 0;
  config_delta_eta_cut = 0;
  config_delta_R_cut = 0;
  config_number_of_fatjet = 0;
  config_fatjet_mass = 0;
}

void analysis::SetConfigSetting(float MET_cut,
                                float METsignificant_cut,
                                TString Isolation,
                                TString Quality,
                                float fatjet_mass_upper_cut,
                                float leading_electron_pt_cut,
                                float delta_eta_cut,
                                float delta_R_cut,
                                int number_of_fatjet,
                                float fatjet_mass){
  config_MET_cut = MET_cut;
  config_METsignificant_cut = METsignificant_cut;
  config_Isolation = Isolation;
  config_Quality = Quality;
  config_fatjet_mass_upper_cut = fatjet_mass_upper_cut;
  config_leading_electron_pt_cut = leading_electron_pt_cut;
  config_delta_eta_cut = delta_eta_cut;
  config_delta_R_cut = delta_R_cut;
  config_number_of_fatjet = number_of_fatjet;
  config_fatjet_mass = fatjet_mass;
}

double analysis::Get_Pvalue(float n_signal, float n_background){
  double pExp = RooStats::NumberCountingUtils::BinomialExpP(n_signal, n_background, 0.5);
  return pExp;
}

double analysis::Get_Zvalue(float n_signal, float n_background){
  double zExp = RooStats::NumberCountingUtils::BinomialExpZ(n_signal, n_background, 0.5);
  return zExp;
}

float analysis::ErrorPropagation_sum(float sigma_one, float sigma_two){
  float sigma_sum = TMath::Sqrt(TMath::Power(sigma_one, 2) + TMath::Power(sigma_two, 2));
  return sigma_sum;
}

float analysis::ErrorPropagation_division(float mu_one, float mu_two, float sigma_one, float sigma_two){
  // mu_one, sigma_one --> Numerator
  // mu_two, sigma_two --> Denominator
  float sigma_division = TMath::Sqrt(TMath::Power((sigma_one/mu_two), 2) + TMath::Power(((mu_one*sigma_two)/TMath::Power(mu_two, 2)), 2));
  return sigma_division;
}

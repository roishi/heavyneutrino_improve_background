#ifndef HEAVYNEUTRINO_IMPROVE_BACKGROUND_ANALYSIS_ONE_RECO_CPP
#define HEAVYNEUTRINO_IMPROVE_BACKGROUND_ANALYSIS_ONE_RECO_CPP

#include "analysis.h"

// There are only functions for 1el + 1lj channel
void analysis::Reset_ALL_one_reco_BKG_SIG(){
  file_bkg_one_reco->Close();
  file_sig_one_reco->Close();

  content_bkg_all_one_reco = 0;
  content_sig_all_one_reco = 0;
  content_data_all_one_reco = 0;

  ljet_el_mass_of_SingleTop_background_one_reco = nullptr;
  ljet_el_mass_of_diboson_background_one_reco = nullptr;
  ljet_el_mass_of_Z_jets_background_one_reco = nullptr;
  ljet_el_mass_of_ttbar_background_one_reco = nullptr;
  ljet_el_mass_of_W_jets_background_one_reco = nullptr;
  ljet_el_mass_of_dijet_background_one_reco = nullptr;
  ljet_el_mass_background_all_one_reco->clear();
}

void analysis::Reset_ALL_one_reco_BKG_SIG_DATA(){
  file_bkg_one_reco->Close();
  file_sig_one_reco->Close();
  file_data_one_reco->Close();

  content_bkg_all_one_reco = 0;
  content_sig_all_one_reco = 0;
  content_data_all_one_reco = 0;

  ljet_el_mass_of_SingleTop_background_one_reco = nullptr;
  ljet_el_mass_of_diboson_background_one_reco = nullptr;
  ljet_el_mass_of_Z_jets_background_one_reco = nullptr;
  ljet_el_mass_of_ttbar_background_one_reco = nullptr;
  ljet_el_mass_of_W_jets_background_one_reco = nullptr;
  ljet_el_mass_of_dijet_background_one_reco = nullptr;
  ljet_el_mass_of_physicsdata_one_reco = nullptr;
  ljet_el_mass_background_all_one_reco->clear();
}

void analysis::Read_from_file_BKG_one_reco(TString input_file_bkg){
  file_bkg_one_reco = TFile::Open(input_file_bkg);
  TString image_name;
  image_name = plot_target + "SingleTop" + plot_suffix;
  ljet_el_mass_of_SingleTop_background_one_reco = (TH1F*)file_bkg_one_reco->Get(image_name);
  ljet_el_mass_background_all_one_reco->push_back(ljet_el_mass_of_SingleTop_background_one_reco);
  image_name = plot_target + "diboson" + plot_suffix;
  ljet_el_mass_of_diboson_background_one_reco = (TH1F*)file_bkg_one_reco->Get(image_name);
  ljet_el_mass_background_all_one_reco->push_back(ljet_el_mass_of_diboson_background_one_reco);
  image_name = plot_target + "Z+jets" + plot_suffix;
  ljet_el_mass_of_Z_jets_background_one_reco = (TH1F*)file_bkg_one_reco->Get(image_name);
  ljet_el_mass_background_all_one_reco->push_back(ljet_el_mass_of_Z_jets_background_one_reco);
  image_name = plot_target + "ttbar" + plot_suffix;
  ljet_el_mass_of_ttbar_background_one_reco = (TH1F*)file_bkg_one_reco->Get(image_name);
  ljet_el_mass_background_all_one_reco->push_back(ljet_el_mass_of_ttbar_background_one_reco);
  image_name = plot_target + "W+jets" + plot_suffix;
  ljet_el_mass_of_W_jets_background_one_reco = (TH1F*)file_bkg_one_reco->Get(image_name);
  ljet_el_mass_background_all_one_reco->push_back(ljet_el_mass_of_W_jets_background_one_reco);
  image_name = plot_target + "dijet" + plot_suffix;
  ljet_el_mass_of_dijet_background_one_reco = (TH1F*)file_bkg_one_reco->Get(image_name);
  ljet_el_mass_background_all_one_reco->push_back(ljet_el_mass_of_dijet_background_one_reco);

  if ((ljet_el_mass_of_SingleTop_background_one_reco == nullptr) || (ljet_el_mass_of_diboson_background_one_reco == nullptr) ||
      (ljet_el_mass_of_Z_jets_background_one_reco == nullptr) || (ljet_el_mass_of_ttbar_background_one_reco == nullptr) ||
      (ljet_el_mass_of_W_jets_background_one_reco == nullptr) || (ljet_el_mass_of_dijet_background_one_reco == nullptr)){
    std::cout << "At least one background process have not ljet-el mass plot (1 electron + 1 learge-R jet channel).\nAbort!!" << std::endl;
    exit(1);
  }
}

void analysis::Read_from_file_SIG_one_reco(TString input_file_sig){
  file_sig_one_reco = TFile::Open(input_file_sig);
  TString image_name;
  image_name = plot_target + "5TeV-200GeV" + plot_suffix; // The file you specified include 5TeV-200GeV images ?
  if (file_sig_one_reco->GetListOfKeys()->Contains(image_name))
    ljet_el_mass_of_signal_one_reco = (TH1F*)file_sig_one_reco->Get(image_name);
  image_name = plot_target + "5TeV-500GeV" + plot_suffix; // The file you specified include 5TeV-500GeV images ?
  if (file_sig_one_reco->GetListOfKeys()->Contains(image_name))
    ljet_el_mass_of_signal_one_reco = (TH1F*)file_sig_one_reco->Get(image_name);

  if (ljet_el_mass_of_signal_one_reco == nullptr){
    std::cout << "The file for signal sample has no ljet-el mass plot.\nAbort!!" << std::endl;
    exit(1);
  }
}

void analysis::Read_from_file_DATA_one_reco(TString input_file_data){
  file_data_one_reco = TFile::Open(input_file_data);
  TString image_name;
  image_name = plot_target + "physicsdata" + plot_suffix;
  ljet_el_mass_of_physicsdata_one_reco = (TH1F*)file_data_one_reco->Get(image_name);

  if (ljet_el_mass_of_physicsdata_one_reco == nullptr){
    std::cout << "The file for physics data has no ljet-el mass plot.\nAbort!!" << std::endl;
    exit(1);
  }
}

void analysis::CheckContent_BKG_one_reco(float integral_lumi_origin, float integral_lumi_ref){
  unsigned int nbin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetNbins();
  float xmin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmin();
  float xmax = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmax();
  float xspan = (xmax - xmin) / nbin;

  content_bkg_all_one_reco = 0; // initialize
  for (unsigned int ibin = 1; ibin < nbin+2; ibin++){
    if (xspan * (ibin-1) >= signal_region_mass_min){
      for (unsigned int hist_index = 0; hist_index < ljet_el_mass_background_all_one_reco->size(); hist_index++){
        content_bkg_all_one_reco = content_bkg_all_one_reco + ljet_el_mass_background_all_one_reco->at(hist_index)->GetBinContent(ibin);
      }
    }
  }
  content_bkg_all_one_reco = content_bkg_all_one_reco * (integral_lumi_ref / integral_lumi_origin);
  content_bkg_all_vector_one_reco.push_back(content_bkg_all_one_reco);
}

void analysis::CheckContent_SIG_one_reco(float integral_lumi_origin, float integral_lumi_ref){
  unsigned int nbin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetNbins();
  float xmin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmin();
  float xmax = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmax();
  float xspan = (xmax - xmin) / nbin;

  content_sig_all_one_reco = 0; // initialize
  for (unsigned int ibin = 1; ibin < nbin+2; ibin++){
    if (xspan * (ibin-1) >= signal_region_mass_min){
      content_sig_all_one_reco = content_sig_all_one_reco + ljet_el_mass_of_signal_one_reco->GetBinContent(ibin);
    }
  }
  content_sig_all_one_reco = content_sig_all_one_reco * (integral_lumi_ref / integral_lumi_origin);
  content_sig_all_vector_one_reco.push_back(content_sig_all_one_reco);
}

void analysis::CheckContent_DATA_one_reco(){
  unsigned int nbin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetNbins();
  float xmin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmin();
  float xmax = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmax();
  float xspan = (xmax - xmin) / nbin;

  content_data_all_one_reco = 0; // initialize
  for (unsigned int ibin = 1; ibin < nbin+2; ibin++){
    if (xspan * (ibin-1) >= signal_region_mass_min){
      content_data_all_one_reco = content_data_all_one_reco + ljet_el_mass_of_physicsdata_one_reco->GetBinContent(ibin);
    }
  }
  content_data_all_vector_one_reco.push_back(content_data_all_one_reco);
}

void analysis::CheckError_BKG_one_reco(float integral_lumi_origin, float integral_lumi_ref){
  unsigned int nbin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetNbins();
  float xmin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmin();
  float xmax = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmax();
  float xspan = (xmax - xmin) / nbin;

  error_bkg_all_one_reco = 0; // initialize
  for (unsigned int ibin = 1; ibin < nbin+2; ibin++){
    if (xspan * (ibin-1) >= signal_region_mass_min){
      for (unsigned int hist_index = 0; hist_index < ljet_el_mass_background_all_one_reco->size(); hist_index++){
        if (error_bkg_all_one_reco == 0)
          error_bkg_all_one_reco = ljet_el_mass_background_all_one_reco->at(hist_index)->GetBinError(ibin);
        else
          error_bkg_all_one_reco = ErrorPropagation_sum(error_bkg_all_one_reco,
                                                        ljet_el_mass_background_all_one_reco->at(hist_index)->GetBinError(ibin));
      }
    }
  }
  error_bkg_all_one_reco = error_bkg_all_one_reco * (integral_lumi_ref / integral_lumi_origin);
  error_bkg_all_vector_one_reco.push_back(error_bkg_all_one_reco);
}

void analysis::CheckError_SIG_one_reco(float integral_lumi_origin, float integral_lumi_ref){
  unsigned int nbin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetNbins();
  float xmin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmin();
  float xmax = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmax();
  float xspan = (xmax - xmin) / nbin;

  error_sig_all_one_reco = 0; // initialie
  for (unsigned int ibin = 1; ibin < nbin+2; ibin++){
    if (xspan * (ibin-1) >= signal_region_mass_min){
      if (error_sig_all_one_reco == 0)
        error_sig_all_one_reco = ljet_el_mass_of_signal_one_reco->GetBinError(ibin);
      else
        error_sig_all_one_reco = ErrorPropagation_sum(error_sig_all_one_reco,
                                                      ljet_el_mass_of_signal_one_reco->GetBinError(ibin));
    }
  }
  error_sig_all_one_reco = error_sig_all_one_reco * (integral_lumi_ref / integral_lumi_origin);
  error_sig_all_vector_one_reco.push_back(error_sig_all_one_reco);

}

void analysis::CheckError_DATA_one_reco(){
  unsigned int nbin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetNbins();
  float xmin = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmin();
  float xmax = ljet_el_mass_of_signal_one_reco->GetXaxis()->GetXmax();
  float xspan = (xmax - xmin) / nbin;

  error_data_all_one_reco = 0;
  for (unsigned int ibin = 1; ibin < nbin+2; ibin++){
    if (xspan * (ibin-1) > signal_region_mass_min){
      if (error_data_all_one_reco == 0)
        error_data_all_one_reco = ljet_el_mass_of_physicsdata_one_reco->GetBinError(ibin);
      else
        error_data_all_one_reco = ErrorPropagation_sum(error_data_all_one_reco,
                                                       ljet_el_mass_of_physicsdata_one_reco->GetBinError(ibin));
    }
  }
  error_data_all_vector_one_reco.push_back(error_data_all_one_reco);
}

void analysis::Calculate_ExpPZ_one_reco(){
  float n_signal = content_sig_all_vector_one_reco.at(content_sig_all_vector_one_reco.size()-1);
  float n_background = content_bkg_all_vector_one_reco.at(content_bkg_all_vector_one_reco.size()-1);
  double pExp = Get_Pvalue(n_signal, n_background);
  double zExp = Get_Zvalue(n_signal, n_background);
  pvalue_vector_one_reco.push_back(pExp);
  zvalue_vector_one_reco.push_back(zExp);
}



#endif
